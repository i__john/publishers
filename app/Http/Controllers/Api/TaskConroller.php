<?php

namespace App\Http\Controllers\Api;

use App\author;
use App\Http\Controllers\Controller;
use App\publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskConroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    //Returns all the publications. June 12,2020
    public function getPublishers()
    {
        $publication = publication::all();

        return $publication;
    }

    //Returns publication by id from the publications. June 12,2020
    public function getPublisher($id)
    {
        $publication = publication::find($id);

        return $publication;
    }

    //Returns author by id from the authors. June 12,2020
    public function getAuthor($id)
    {
        $author = author::find($id);

        return $author;
    }

    //Returns all the authors. June 12,2020
    public function getAuthors()
    {
        $author = author::all();

        return $author;
    }

    public function allPublications()
    {
        $publications=publication::join('authors', 'publications.author', '=', 'authors.id')->select('publications.*', 'authors.name as aname')->get();
        return $publications;
    }

    //Logout Method, Also could have done it in a separate controller
    public function logout()
    {
        Auth::user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
