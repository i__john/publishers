<?php

namespace App\Http\Controllers\Auth\Login;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;;



class LoginController extends DefaultLoginController
{

    public function __construct()
    {
        $this->middleware('guest:author')->except('logout');
    }
    public function showLoginForm()
    {
        return view('auth.authorLogin');
    }

    protected function guard()
    {
        return Auth::guard('author');
    }
}
