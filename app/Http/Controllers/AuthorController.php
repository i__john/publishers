<?php

namespace App\Http\Controllers;

use App\author;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class authorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web,author');
    }
    public function view()
    {
        if(Auth::user()->role =="admin") {
        $author = author::paginate(2);
        return view('author', compact('author'));
        }
        else
            return redirect( route('home'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|',
            'email'=>'required|',
            'address'=>'required|',
            'password'=>'required|',
            'username'=>'required|',
            'dateOfBirth'=>'required'
        ]);
        author::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'address'=> $request['address'],
            'dateOfBirth' => $request['dateOfBirth'],
            'username' => $request['username'],
            'password' => Hash::make($request['password']),
            'role'=>"author",
        ]);
        $name=$request->name;
        $email=$request->email;
        $data = array('name'=>$name);
        Mail::send(['text'=>'mailAuthor'], $data, function($message) use($email, $name) {
            $message->to($email, $name)->subject
            ('New Account Added');
            $message->from('admin@apploix.com','AppLogix');
        });
        return redirect(route('author'));
    }

    public function storeView()
    {
        if(Auth::user()->role =="admin") {
        return view('author-in');
        }
        else
            return redirect( route('home'));
    }

    public function delete(Request $request)
    {
        if(Auth::user()->role =="admin") {

            $author = author::findOrFail($request->id);
            $author->delete();

            return redirect(route('author'));
        }
        else
            return redirect( route('home'));

    }

    public function get(Request $request)
    {
        $author = $author = author::findOrFail($request->id);
        return $author -> toJson();
    }

    public function update(Request $request)
    {
        $author = author::find($request->id);
        $author->update($request->all());
        return redirect(route('author'));
    }
}
