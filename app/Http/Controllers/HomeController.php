<?php

namespace App\Http\Controllers;

use App\author;
use App\publication;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web,author');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $author = author::all()->count();
        $publication=publication::all()->count();
        $admin=user::all()->count()+author::all()->count();
        return view('home',compact('author','publication', 'admin'));
    }
}
