<?php

namespace App\Http\Controllers;

use App\author;
use App\publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class publicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web,author');
    }
    public function view()
    {
        $author = author::all();
        if(Auth::User()->role=="admin")
        {
            $publication = publication::paginate(2);
        }
        else
        {
            $email=Auth::User()->email;
            $id = author::where('email',$email)->first()->id;
            $publication = publication::where('author',$id)->paginate(2);
        }
        return view('book', compact('author','publication'));

    }

    public function store(Request $request)
    {
        $author=author::where('id',$request['author'])->first();
        $email=$author['email'];
        $name=$author->name;
        $data = array('name'=>$name, 'publication'=>$request->name);
        Mail::send(['text'=>'mail'], $data, function($message) use($email, $name) {
            $message->to($email, $name)->subject
            ('New Publication Added');
            $message->from('admin@apploix.com','AppLogix');
        });

        publication::create($request->all());
        return redirect(route('publication'));
    }

    public function storeView()
    {
        if(Auth::user()->role =="admin") {
            $author = author::all();
        }
        else
            $author = author::where('email',Auth::user()->email)->get();
        return view('book-in',compact('author'));
    }

    public function delete(Request $request)
    {
        $publication = publication::findOrFail($request->id);
        $publication->delete();

        return redirect(route('publication'));
    }

    public function get(Request $request)
    {
        $publication = publication::findOrFail($request->id);
        return $publication -> toJson();
    }

    public function update(Request $request)
    {
        $publication = publication::find($request->id);
        $publication->update($request->all());
        return redirect(route('publication'));
    }
}
