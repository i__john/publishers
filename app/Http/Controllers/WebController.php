<?php

namespace App\Http\Controllers;

use App\publication;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        $publications=publication::join('authors', 'publications.author', '=', 'authors.id')->select('publications.*', 'authors.name as aname')->get();
        return view('welcome',compact('publications'));
    }
}
