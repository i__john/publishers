@extends('main')
@section('content')

    {{csrf_field()}}
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Authors</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Authors</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    <!-- /.card -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="/author-in" class="btn btn-dark">Add Authors</a>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Date Of Birth</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($author as $authors)
                                    <tr>
                                        <td>{{$authors->name}}</td>
                                        <td>{{$authors->email}}</td>
                                        <td>{{$authors->address}}</td>
                                        <td>{{$authors->dateOfBirth}}</td>
                                        <td><Button class="btn btn-dark myModaledit" data-toggle="modal" data-target="#myModaledit" data-id="{{$authors->id}}">Update</Button>&nbsp&nbsp<Button class="btn btn-danger myModalDelete" data-toggle="modal" data-target="#myModalDelete" data-id="{{$authors->id}}">Delete</Button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Date Of Birth</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                            {{$author->render()}}
                        </div><!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>


    <div class="modal fade" id="myModaledit" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Author<span></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form action="/author-up" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" id="id" name="id" placeholder="name">
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="name">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="date" class="form-control" name="dateOfBirth" id="dateOfBirth" placeholder="Date Of Birth">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalDelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Author<span></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form action="/author-del" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" id="id1" disabled>
                                <input type="text" class="form-control" id="id2" name="id" hidden>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" id="name1" placeholder="name" disabled>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>
        </div>
    </div>

    @endsection
<script
    src="https://code.jquery.com/jquery-3.5.1.js"
    integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('.myModaledit').click(function() {
            var id=$(this).data('id');
            var url = '{{route('author-get')}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : 'get',
                url  : url,
                data : {'id':id},
                success:function(data){
                    var obj = JSON.parse(data);
                    $('#id').val(obj.id);
                    $('#name').val(obj.name);
                    $('#email').val(obj.email);
                    $('#address').val(obj.address);
                    $('#dateOfBirth').val(obj.dateOfBirth);

                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('.myModalDelete').click(function() {
            var id=$(this).data('id');
            var url = '{{route('author-get')}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : 'get',
                url  : url,
                data : {'id':id},
                success:function(data){
                    var obj = JSON.parse(data);
                    $('#id1').val(obj.id);
                    $('#id2').val(obj.id);
                    $('#name1').val(obj.name);
                }
            });
        });
    });
</script>

