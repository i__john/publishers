@extends('main')
@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add Publication</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="/publication-in" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Author</label>
                                        <select class="form-control" name="author" id="author">
                                            <option>Select Author....</option>
                                            @foreach($author as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Date Of Release</label>
                                        <input type="date" class="form-control" name="releaseDate" id="dateOfRelease" placeholder="Date Of Release" required>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.card -->
    </div>


@endsection
