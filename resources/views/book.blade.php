@extends('main')
@section('content')


    {{csrf_field()}}
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Publications</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Publications</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.card -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="/publication-in" class="btn btn-dark">Add Publications</a>
                            </div>
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Author</th>
                                        <th>Date Of Release</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($publication as $row)
                                        <tr>
                                            <td>{{$row->name}}</td>
                                            @foreach($author as $row1)
                                                @if($row1->id == $row->author)
                                            <td>{{$row1->name}}</td>
                                                @endif
                                            @endforeach
                                            <td>{{$row->releaseDate}}</td>
                                            <td><Button class="btn btn-dark myModaledit" data-toggle="modal" data-target="#myModaledit" data-id="{{$row->id}}">Update</Button>&nbsp&nbsp<Button class="btn btn-danger myModalDelete" data-toggle="modal" data-target="#myModalDelete" data-id="{{$row->id}}">Delete</Button></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Author</th>
                                        <th>Date Of Release</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                {{$publication->render()}}
                            </div><!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="modal fade" id="myModaledit" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Publication<span></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form action="/publication-up" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" id="id" name="id" placeholder="name">
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="name">
                            </div>
                            <div class="form-group">
                                <label>Author</label>
                                <select class="form-control" name="author" id="author">
                                    @foreach($author as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Date Of Release</label>
                                <input type="date" class="form-control" name="releaseDate" id="dateOfRelease" placeholder="Date Of Release">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalDelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Publication<span></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form action="/publication-del" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Id</label>
                                <input type="text" class="form-control" id="id1"  placeholder="ID" disabled>
                                <input type="text" class="form-control" id="id2" name="id" placeholder="name" hidden>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control"  id="name1" placeholder="name" disabled>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>
        </div>
    </div>

@endsection
<script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('.myModaledit').click(function() {
            var id=$(this).data('id');
            alert(id);
            var url = '{{route('publication-get')}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : 'get',
                url  : url,
                data : {'id':id},
                success:function(data){

                    var obj = JSON.parse(data);
                    $('#id').val(obj.id);
                    $('#name').val(obj.name);
                    $('#author').val(obj.author);
                    $('#dateOfRelease').val(obj.releaseDate);

                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('.myModalDelete').click(function() {
            var id=$(this).data('id');
            alert(id);
            var url = '{{route('publication-get')}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : 'get',
                url  : url,
                data : {'id':id},
                success:function(data){
                    var obj = JSON.parse(data);
                    $('#id1').val(obj.id);
                    $('#id2').val(obj.id);
                    $('#name1').val(obj.name);
                }
            });
        });
    });
</script>
