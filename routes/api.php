<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//User Login and logout using passport authentication
Route::post('/login', 'Api\ApiController@login');
Route::post('/logout', 'Api\TaskConroller@logout');

//Tasks for the api
Route::get('/publications', 'Api\TaskConroller@getPublishers');
Route::get('/publications/{id}', 'Api\TaskConroller@getPublisher');
Route::get('/authors', 'Api\TaskConroller@getAuthors');
Route::get('/authors/{id}', 'Api\TaskConroller@getAuthor');
Route::get('/all', 'Api\TaskConroller@allPublications');
