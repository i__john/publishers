<?php

use App\publication;
use Illuminate\Support\Facades\Route;

Route::get('/', 'WebController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/author', 'AuthorController@view')->name('author');
Route::get('/author-in', 'AuthorController@storeView');
Route::post('/author-in','AuthorController@store');
Route::post('/author-up','AuthorController@update');
Route::post('/author-del', 'AuthorController@delete');
Route::get('/author-get', 'AuthorController@get')->name('author-get');

Route::get('/publication', 'PublicationController@view')->name('publication');
Route::get('/publication-in', 'PublicationController@storeView');
Route::post('/publication-in','PublicationController@store');
Route::post('/publication-up','PublicationController@update');
Route::post('/publication-del', 'PublicationController@delete');
Route::get('/publication-get', 'PublicationController@get')->name('publication-get');

Route::namespace('Auth\Login')
    ->group(function() {
        Route::post('Author/Login', 'LoginController@login');
        Route::get('Author/Login', 'LoginController@showLoginForm');
    });

